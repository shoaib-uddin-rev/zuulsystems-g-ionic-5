import { Component, OnInit } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';
@Component({
  selector: 'app-components-notification-item',
  templateUrl: './components-notification-item.component.html',
  styleUrls: ['./components-notification-item.component.scss'],
})
export class ComponentsNotificationItemComponent implements OnInit {

  @Input() itm: any;
  @Output('removeNotf') removeNotf: EventEmitter<any> = new EventEmitter<any>();
  @Output('rejectNotf') rejectNotf: EventEmitter<any> = new EventEmitter<any>();
  @Output('createPass') createPass: EventEmitter<any> = new EventEmitter<any>();
  @Output('showPass') showPass: EventEmitter<any> = new EventEmitter<any>();
  @Output('markasread') markasread: EventEmitter<any> = new EventEmitter<any>();
  text: string;
  public show = false;
  countdown;


  constructor() { }

  ngOnInit(): void {

    if(!this.itm.status){
      this.itm.status = 0;
    }

    if(this.itm.expire_at != null && this.itm.anonymous == 1){
      this.countdown = this.itm.expire_at
        // // console.log(this.itm.expire_at);
        let intervel = setInterval( () => {

          this.countdown--;
          if(this.countdown < 1){
            clearInterval(intervel);
            if(this.itm.type == 0){
              this.rejectNotf.emit(this.itm);
            }else{
              this.removeNotf.emit(this.itm);
            }

          }

        }, 1000)

    }

    // setTimeout( () => {
    //   this.markasread.emit(this.itm['id']);
    // }, 2000);

  }

  // convertSecondstoTime(giventime) {

  //   var nd = giventime.replace(' ', 'T');
  //   // console.log(nd);
  //   var t1 = new Date(nd);;
  //   var t2 = new Date();
  //   // console.log(t1, t2);
  //   var dif = t1.getTime() - t2.getTime();
  //   // console.log(dif);

  //   var Seconds_from_T1_to_T2 = dif / 1000;
  //   return Math.round(Seconds_from_T1_to_T2);

  // }

  convertSecondstoTime(given_seconds) {

    let dateObj = new Date(given_seconds * 1000);
    let minutes = dateObj.getUTCMinutes();
    let seconds = dateObj.getSeconds();

    let timeString = minutes.toString().padStart(2, '0') + ':' + seconds.toString().padStart(2, '0');
    return timeString;


  }

}
