import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { ComponentsNotificationItemComponent } from './components-notification-item.component';
import { IonicModule } from '@ionic/angular';

@NgModule({
	declarations: [
		ComponentsNotificationItemComponent
	],
	imports: [
		CommonModule,
    IonicModule,
    NgModule,
    BrowserModule
	],
	exports: [
		ComponentsNotificationItemComponent
	]
})
export class NotificationItemComponentModule {}
