import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-tutorial-cards',
  templateUrl: './tutorial-cards.component.html',
  styleUrls: ['./tutorial-cards.component.scss'],
})
export class TutorialCardsComponent implements OnInit {

  @Input() heading;
  @Input() subheading;
  @Input() image;
  
  constructor() { }

  ngOnInit() {}

}
