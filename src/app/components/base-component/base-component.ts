import { NavService } from './../../services/nav.service';
import { Injector } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Platform } from '@ionic/angular';
import { ModalService } from 'src/app/services/basic/modal.service';
import { PopoversService } from 'src/app/services/basic/popovers.service';
import { EventsService } from 'src/app/services/events.service';
import { NetworkService } from 'src/app/services/network.service';
import { SqliteService } from 'src/app/services/sqlite.service';
import { UserService } from 'src/app/services/user.service';
import { UtilityService } from 'src/app/services/utility.service';
import { Location } from '@angular/common';

export abstract class BaseComponent {

    public network: NetworkService;
    public utility: UtilityService;
    public nav: NavService;
    public location: Location;
    public events: EventsService;
    public platform: Platform;
    public sqlite: SqliteService;
    public formBuilder: FormBuilder;
    public popover: PopoversService;
    public users: UserService;
    public modals: ModalService;


    constructor(injector: Injector) {
        this.platform = injector.get(Platform);
        this.sqlite = injector.get(SqliteService);
        this.network = injector.get(NetworkService);
        this.utility = injector.get(UtilityService);
        this.location = injector.get(Location);
        this.events = injector.get(EventsService);
        this.nav = injector.get(NavService);
        this.formBuilder = injector.get(FormBuilder);
        this.users = injector.get(UserService);
    }

    
}