import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../../base-page/base-page';

@Component({
  selector: 'app-welcomescreen',
  templateUrl: './welcomescreen.component.html',
  styleUrls: ['./welcomescreen.component.scss'],
})
export class WelcomescreenComponent extends BasePage implements OnInit {

  constructor(injector: Injector) { 
    super(injector);
  }

  ngOnInit() {}

  closeModal() {
    this.modals.dismiss();
  }

}
