import { Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from '../../base-page/base-page';

@Component({
  selector: 'app-successscreen',
  templateUrl: './successscreen.component.html',
  styleUrls: ['./successscreen.component.scss'],
})
export class SuccessscreenComponent extends BasePage implements OnInit {

  @Input() heading1;
  @Input() Start;
  constructor(injector: Injector) { 
    super(injector);
  }

  ngOnInit() {}

  dashboardopen(){
    this.modals.dismiss().then( () => {
      this.events.publish("registration:finish");
    })
    
  }

}
