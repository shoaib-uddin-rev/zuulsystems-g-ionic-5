import { Component, OnInit, AfterViewInit, Injector } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';
import {DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-guard-licence-page',
  templateUrl: './guard-licence-page.component.html',
  styleUrls: ['./guard-licence-page.component.scss'],
})
export class GuardLicencePageComponent extends BasePage implements OnInit, AfterViewInit {

  aForm: FormGroup;
  submitAttempt = false;
  liImageBase64 = "";
  profile: any;
  user: any;
  tempaccesstoken: any;
  date_of_birth_mdy = "";
  revisit: boolean = false;
  
  constructor(injector: Injector, public _DomSanitizer: DomSanitizer,) {
    super(injector);
    this.initialize()
  }

  ngOnInit() {}

  ngAfterViewInit() {
    
  }

  async initialize(){
    this.user = this.sqlite.getActiveUser();
    console.log(this.user)
    this.setupForm();
  }

  setupForm(){

    var re = /\S+@\S+\.\S+/;

    this.aForm = this.formBuilder.group({
      licence_format: ['dl', Validators.compose([Validators.required])],
      licence_image: ['', Validators.compose([Validators.required])],
    });

  }

  getlicenceimage(){

    this.utility.snapImage("licence").then(image => {
      this.liImageBase64 = image as string;
      this.aForm.controls['licence_image'].setValue(this.liImageBase64);
    })

  }

  finishregister(){

    var in_licence_image = !this.aForm.controls.licence_image.valid
    var in_licence_format = !this.aForm.controls.licence_format.valid

    var eror = "Please enter valid";
    if(in_licence_format){ this.utility.showAlert( eror +" license format"); return }
    if(in_licence_image){ this.utility.showAlert( "Please Upload License Image"); return }



    this.submitAttempt = true;
    var formdata = this.aForm.value;
    formdata["id"] = this.user.user_id;

    this.utility.showLoader();

    this.utility.convertImageUrltoBase64(formdata.licence_image)
    .then(image => {
      formdata['licence_image'] = image
      formdata['licence_via_guard'] = true,
      formdata['scanlog_id'] = this.user['scanlog_id'];
      console.log("scanlog_id", formdata['scanlog_id']);
      
      this.utility.hideLoader();

      this.network.updateProfileById(formdata["id"], formdata).then(async (res) => {

        await this.utility.showAlert('License Image uploaded, thank you for cooperation', 'Upload Successful');
        this.closeDismiss(res);

      })
    })



  }

  closeDismiss(res){
    this.modals.dismiss(res);
  }



}
