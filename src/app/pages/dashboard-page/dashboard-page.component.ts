
import { Component, Injector, OnInit, AfterViewInit } from '@angular/core';
import { ParentalNotificationItemComponentModule } from 'src/app/components/parental-notification-item/parental-notification-item.module';
import { BasePage } from '../base-page/base-page';
import { UpdatePasswordPageComponent } from '../update-password-page/update-password-page.component';
import { CodeVerificationPageComponent } from '../code-verification-page/code-verification-page.component';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { GuardLicenceImageComponent } from '../guard-licence-image/guard-licence-image.component';
import { GuardResidentsPageComponent } from '../guard-residents-page/guard-residents-page.component';
import { RegisterPageComponent } from '../register-page/register-page.component';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.scss'],
})
export class DashboardPageComponent extends BasePage implements OnInit {

  avatar = this.users.avatar;
  canBeResident: boolean = false;
  isUserLoaded: boolean = false;
  user: any;
  plist: any[] = [];
  storedData;
  offset = 0;
  rnotif: boolean = false;
  contact_count = 0;
  getNotified: boolean = false;
  isEmailVerificationPending: boolean = false;
  suspandAccount: boolean = false;
  disableCreatePass: boolean = true;
  notification_number = -1;
  searchTerm = '';

  constructor(injector: Injector, private barcodeScanner: BarcodeScanner,) {
    super(injector);

    this.initializeView();
    this.events.subscribe('dashboard:initialize', this.initializeView.bind(this));
    this.events.subscribe('dashboard:updateprofile', this.openRegistrationPage.bind(this));
      
  }

  ngOnInit() {

  }

  AfterViewInit(){
    
  }

  openRegistrationPage(){
    this.menuCtrl.close();
    this.modals.present(RegisterPageComponent, { new: false, user: this.user })
  }

  async initializeView(){

    var self = this;
    this.user = await this.sqlite.getActiveUser();
    this.users._user = this.user;
    this.menuCtrl.enable(true, 'authenticated');
    // sync contact list from global database to local database,
    // if sync already done skip the step

    this.getScanHistory().then(() => {

      if (this.user["is_reset_password"] == 1) {
        this.modals.present(UpdatePasswordPageComponent, { user: this.user })
      }
      
    });
    
    this.events.publish('user:settokentoserver');

    // this.network.getVendorList().subscribe( async v => {
    //   await this.sqlite.setVendorListInDatabase(v["vendors"]);
    // })

    // if(this.suspandAccount == true){
    //   this.utility.presentFailureToast("Your Account Is Suspended, Please Contact You Admin ");
    //   return;
    // }

  }

  ionViewWillLeave() {
  }


  emptyarray(){
    var self = this;
    return new Promise( resolve => {
      self.plist = [];
      resolve();
    })
  }

  async presentPopover(ev) {

    this.getNotified = false;
    this.rnotif = false;

  }

  async presentPopoverParental(){
    this.getNotified = false;
    this.rnotif = false;
    const data = await this.modals.present(ParentalNotificationItemComponentModule);
  }

  doRefresh($event = null) {

    this.notification_number = -1;

    return new Promise( async resolve => {
      this.offset = 0;
      this.plist = [];
      this.storedData = {};
      if($event){
        $event.target.complete();
      }
      this.getScanHistory( this.storedData );
      // await this.callAnnouncements();
      

      resolve();
    })


  }

  getScanHistory( data = {}, loader = true, search = false ) {

    return new Promise((resolve) => {1

      if ( this.offset == -1 ) {
        resolve();
      }  else {
        data['offset'] = this.offset;
        
        this.network.getScanHistory(data, loader).then((res: any) => {
          // console.log(data);

          if(search){
            this.plist = res['logs'];
          }else{
            if(this.offset == 0){
              this.plist = res['logs'];
            }else{
              this.plist = this.plist.concat(res['logs']);
            }
          }
          
          

          this.offset = res['offset'];
          resolve();
        })
      }

    })
  }


  logout() {
    this.events.publish('user:logout');
  }

  async openLogUsers($event) {
    // var r = item;
    var self = this;
    const _data = await this.popover.present($event,{ 
      id: null,
      flag: "SW"
    });

    const data = _data.data;

    if (data == null) {
      return;
    }

    console.log(data);
    let sw_user = data["param"];
    console.log(sw_user);
    this.users.switchUserAccount(sw_user['id']).then( data => {
      if(!data){
        this.nav.push('LoginPage', {sw_user: sw_user});
      }
    })
  }


  loadMore($event){

    this.getScanHistory(this.storedData, false).then( v => {
      $event.complete();
    });


  }

  async scanCode(istext: boolean = false) {

    let qrcode = istext ? await this.utility.presentInput('Ok','Cancel', 'Input QRCode') : await this.barcodeScanner.scan() ;
    if(!qrcode) return;

    qrcode.then(barcodeData => {

      if(!barcodeData.text) return;

      var data = { qrcode: barcodeData.text }
      this.network.validateQrCode(barcodeData.text).then(async res => {
        console.log(res);

        let alertmode = res['alertmode'];
        console.log(alertmode);
        if(alertmode['flag'] == true){
          await this.utility.showAlert(alertmode['message']);
        }

        this.getScanHistory();

        var data = res['data'];
        console.log(data);
        // send scanlog_id
        this.gotoGuardLicenceImage(data);
      },
      err => {
        console.log(err);
        this.getScanHistory();
      });

    }, (err) => {
        console.log('Error: ', err);
    });
  }

  async gotoGuardLicenceImage(data){
    const _data = await this.modals.present(GuardLicenceImageComponent, {data: data});

  }

  // Init a timeout variable to be used below
  timeout = null;
  setFilteredItems($event, flag){
    var self = this;
    const search = $event.target.value;

    clearTimeout(self.timeout);
    // Make a new timeout set to go off in 1000ms (1 second)
    
    let data = {
      'search': search
    }

    self.timeout = setTimeout( () => {
      console.log('Input Value:', search);
      self.getScanHistory(data, false, true);
    }, 700);

  }

  gotiGuardRes(){
    this.nav.push('GuardResidentsPage');
  }



}
