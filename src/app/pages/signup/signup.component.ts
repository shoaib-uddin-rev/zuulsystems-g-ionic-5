import { Component, OnInit, Injector } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
})
export class SignupComponent extends BasePage implements OnInit {


  aForm: FormGroup;
  submitAttempt = false;
  user: any;
  isfbSignup: boolean = false;
  isgSignup: boolean = false;
  isSocialSignup: boolean = false;
  hideForm: boolean = false;
  socialImage;

  constructor(injector: Injector) { 
    super(injector);
    this.setupForm();
  }

  ngOnInit() {}

  setupForm() {

    var re = /\S+@\S+\.\S+/;

    this.aForm = this.formBuilder.group({
      email: [''],
      name: ['', Validators.compose([Validators.minLength(3), Validators.maxLength(30), Validators.pattern('[a-zA-Z ]*'), Validators.required])],
      phone_number: ['', Validators.compose([ Validators.required]) ],
      password: ['', Validators.compose([Validators.minLength(9), Validators.maxLength(30), Validators.required, Validators.pattern(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/)])],
      password_confirm: ['', Validators.compose([Validators.minLength(6), Validators.maxLength(30), Validators.required])],
      profile_image: ['']
    }, { validator: this.utility.checkIfMatchingPasswords('password', 'password_confirm') })


  }

  login() {
    this.nav.setRoot('1/LoginPage', {
      animate: true,
      direction: 'forward'
    });
  }

  EsingUp(){

    this.submitAttempt = true;

    var in_name = !this.aForm.controls.name.valid || !this.utility.isLastNameExist(this.aForm.controls.name.value);
    var in_phone = !this.aForm.controls.phone_number.valid;
    in_phone = !this.utility.isPhoneNumberValid(this.aForm.controls.phone_number.value);
    var in_password = !this.aForm.controls.password.valid
    var in_cpassword = !this.aForm.controls.password_confirm.valid

    if(in_name){
      this.utility.presentFailureToast("Name/Full Name is required")
      return
    }

    if(in_phone){
      this.utility.presentFailureToast("Phone Number required")
      return
    }

    if(in_password){
      this.utility.presentFailureToast("Valid Password Required")
      return
    }

    var _p = this.aForm.controls.password.value;
    var _cp = this.aForm.controls.password_confirm.value;


    if(_p != _cp){
      this.utility.presentFailureToast("confirm password must match password field")
      return
    }

    var formdata = this.aForm.value;
    formdata['profile_image'] = null;
    this.doSignup(formdata)

  }

  doSignup(formdata) {

    formdata['register_with_phonenumber'] = true;
    this.network.register(formdata).then(() => {
      formdata['showelcome'] = true;
      this.events.publish('user:login', formdata)
    }, err => {
      // console.log(err.message);
    })

  }

  onTelephoneChange(ev, tel) {
    if (ev.inputType != "deleteContentBackward") {
      var _tel = this.utility.onkeyupFormatPhoneNumberRuntime(tel, false);
      // console.log(_tel);
      this.aForm.controls["phone_number"].setValue(_tel);
    }
  }


}
