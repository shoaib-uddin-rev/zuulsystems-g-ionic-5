import { GuardLicenceImageComponent } from './guard-licence-image/guard-licence-image.component';

import { ImageCropperModule } from 'ngx-image-cropper';
import { TutorialCardsComponentModule } from './../components/tutorial-cards/tutorial-cards-component.module';
import { CommonModule, Location } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { SqliteService } from '../services/sqlite.service';
import { DashboardPageComponent } from './dashboard-page/dashboard-page.component';
import { ForgetPasswordPageComponent } from './forget-password-page/forget-password-page.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { PagesRoutingModule } from './pages-routing.module';
import { SignupComponent } from './signup/signup.component';
import { SplashPageComponent } from './splash-page/splash-page.component';
import { TutorialPageComponent } from './tutorial-page/tutorial-page.component';
import { ContactItemComponent } from '../components/contact-item/contact-item.component';
import { GmapPageComponent } from './gmap-page/gmap-page.component';
import { CreateVehiclePageComponent } from './create-vehicle-page/create-vehicle-page.component';
import { ComponentsNotificationItemComponent } from '../components/components-notification-item/components-notification-item.component';
import { ParentalNotificationItemComponent } from '../components/parental-notification-item/parental-notification-item.component';
import { CodeVerificationPageComponent } from './code-verification-page/code-verification-page.component';
import { CountryCodePageComponent } from './country-code-page/country-code-page.component';
import { CropperComponent } from '../components/cropper/cropper.component';
import { EmptyviewComponentModule } from '../components/emptyview/emptyview.component.module';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
import { GuardResidentsPageComponent } from './guard-residents-page/guard-residents-page.component';
import { QuickPassPageComponent } from './quick-pass-page/quick-pass-page.component';
import { GuardLicencePageComponent } from './guard-licence-page/guard-licence-page.component';
import { ContactpopoverPageComponent } from '../components/contactpopover-page/contactpopover-page.component';
import { FirstscreenComponent } from './register-page/firstscreen/firstscreen.component';
import { RegisterPageComponent } from './register-page/register-page.component';
import { SecondscreenComponent } from './register-page/secondscreen/secondscreen.component';
import { SuccessscreenComponent } from './register-page/successscreen/successscreen.component';
import { ThirdscreenComponent } from './register-page/thirdscreen/thirdscreen.component';
import { WelcomescreenComponent } from './register-page/welcomescreen/welcomescreen.component';

@NgModule({
    declarations: [
        SplashPageComponent,
        SignupComponent,
        ForgetPasswordPageComponent,
        
        DashboardPageComponent,

        // WelcomePageComponent,
        // RegisterPageComponent,
        LoginPageComponent,
        // ForgetPasswordPageComponent,
        TutorialPageComponent,
        ContactItemComponent,
        GmapPageComponent,
        CreateVehiclePageComponent,
        ParentalNotificationItemComponent,
        ComponentsNotificationItemComponent,
        CodeVerificationPageComponent,
        CountryCodePageComponent,
        CropperComponent,
        GuardLicenceImageComponent,
        GuardResidentsPageComponent,
        QuickPassPageComponent,
        GuardLicencePageComponent,
        ContactpopoverPageComponent,
        RegisterPageComponent,
        WelcomescreenComponent,
        FirstscreenComponent,
        SecondscreenComponent,
        ThirdscreenComponent,
        SuccessscreenComponent,
        // DashboardPageComponent,
    ],
    imports: [
        CommonModule,
        PagesRoutingModule,
        // NgxPubSubModule,
        FormsModule,
        ReactiveFormsModule,
        IonicModule,
        TutorialCardsComponentModule,
        ImageCropperModule,
        EmptyviewComponentModule,
        NgxQRCodeModule

        // TutorialCardsComponentModule
        // NotificationItemComponentModule,
        // ParentalNotificationItemComponentModule
    ], 
    exports: [
    ],
    providers: [
        SqliteService,
        Location,
    ]
})
export class PagesModule{}