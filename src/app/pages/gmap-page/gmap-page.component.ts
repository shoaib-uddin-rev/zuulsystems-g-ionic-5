import { Component, ElementRef, Injector, Input, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';
declare var google;

@Component({
  selector: 'app-gmap-page',
  templateUrl: './gmap-page.component.html',
  styleUrls: ['./gmap-page.component.scss'],
})
export class GmapPageComponent extends BasePage implements OnInit, AfterViewInit {

  @ViewChild('map', { static: false }) mapElement: ElementRef;
  @ViewChild('searchbox', { read: ElementRef }) searchbar: ElementRef;
  map: any;
  start = 'chicago, il';
  end = 'chicago, il';
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;
  myLatLng: any;

  @Input() myAddress: any;
  mmarker: any;
  paddress: string;

  searchQuery: string = '';
  items: string[];
  @Input() newAddress = false;
  @Input() isDirections = false;
  isBothDirectionsAvailable = false;
  iOrigin;
  iDestination;

  constructor(injector: Injector) {
    super(injector);

  }

  ngOnInit() { }

  ngAfterViewInit() {

    this.platform.ready().then(() => {
      this.initializeMapBeforeSetCoordinates().then(v => {
        this.initMap(v);

        setTimeout(async () => {

          let active = await this.users.getFlag('google_map');
          if (!active) {
            let flag = await this.utility.presentConfirm("Thanks", "Remind Later", "How To Use", "In the search field, type in the new address and press enter. You can also hold your finger and drag to new location, then press the + symbol in the top right corner");
            this.users.setFlag('google_map', flag);
    
          }
    
        }, 1000)

      });
    })

    
  }

  initializeMapBeforeSetCoordinates() {

    return new Promise(async resolve => {
      let mylocation = await this.utility.getCurrentLocationCoordinates();
      console.log("log-B", mylocation);
      this.map = new google.maps.Map(this.mapElement.nativeElement, {
        zoom: 13,
        center: mylocation
      });

      console.log(this.myAddress);
      this.utility.getCoordsForGeoAddress(this.myAddress)
        .then(coords => {
          console.log("loa-A", coords);
          resolve({
            'mylocation': mylocation,
            'destinatioLocation': coords
          })
        }, err => {
          console.log(err);
          this.utility.presentFailureToast("Destination Not Found for given address")
          resolve({
            'mylocation': mylocation,
            'destinatioLocation': null
          })
        })

    })

  }


  initMap(val) {

    // this.map = new google.maps.Map(this.mapElement.nativeElement, {
    //   zoom: 13,
    //   center: localatlong
    // });

    if (this.isDirections == false) {
      this.mmarker = new google.maps.Marker({
        position: val['mylocation'],
        map: this.map,
        draggable: true,
        title: 'Destination'
      });

      google.maps.event.addListener(this.mmarker, 'dragend', function (event) {
        var lt = event.latLng.lat();
        var lg = event.latLng.lng();
        var coords = { lat: lt, lng: lg };
        self.getGeoAddress(coords)
      });

      this.setSearchBox()

    }


    var self = this;
    this.directionsDisplay.setMap(this.map);



    // console.log(this.isDirections);
    if (this.isDirections == true) {

      var destination = new google.maps.LatLng(val['destinatioLocation']);
      var mylocation = new google.maps.LatLng(val['mylocation']);

      console.log("d-", destination, mylocation);

      var request = {
        origin: val['mylocation'],
        destination: val['destinatioLocation'],
        travelMode: 'DRIVING',
        drivingOptions: {
          departureTime: new Date(Date.now() + 2000),  // for the time N milliseconds from now.
          trafficModel: 'bestguess'
        }




        // drivingOptions: DrivingOptions,
        // unitSystem: UnitSystem,
        // waypoints[]: DirectionsWaypoint,
        // optimizeWaypoints: Boolean,
        // provideRouteAlternatives: Boolean,
        // avoidFerries: Boolean,
        // avoidHighways: Boolean,
        // avoidTolls: Boolean,
        // region: String
      }

      self.directionsService.route(request, function (result, status) {
        // console.log(status);
        if (status == "OK") {
          self.directionsDisplay.setDirections(result);
          self.isBothDirectionsAvailable = true;
          self.iOrigin = mylocation;
          self.iDestination = destination;
        } else {
          console.log(result, status)
          self.utility.presentFailureToast("Error Finding Driving Directions on Map");

          self.addDestinationOriginMarkers(val['mylocation'], val['destinatioLocation'])

        }
      })

    }

    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.





  }

  addDestinationOriginMarkers(origin, destination) {
    var marker_o = new google.maps.Marker({
      position: new google.maps.LatLng(origin['lat'], origin['lng']),
      title: "Origin"
    });



    var marker_d = new google.maps.Marker({
      position: new google.maps.LatLng(destination['lat'], destination['lng']),
      title: "Destination"
    });

    marker_o.setMap(this.map);
    marker_d.setMap(this.map);

    var bounds = new google.maps.LatLngBounds();
    bounds.extend(origin);
    bounds.extend(destination);
    this.map.fitBounds(bounds);


  }

  setSearchBox() {

    var self = this;
    var searchInput = this.searchbar.nativeElement.querySelector('.searchbar-input');
    // console.log("Search input", searchInput);
    var searchBox = new google.maps.places.SearchBox(searchInput);
    var markers = [];

    searchBox.addListener('places_changed', function () {
      var places = searchBox.getPlaces();
      if (places.length == 0) {
        return;
      }

      self.mmarker.setMap(null);
      markers = [];

      // For each place, get the icon, name and location.
      var bounds = new google.maps.LatLngBounds();
      places.forEach(function (place) {
        if (!place.geometry) {
          // console.log("Returned place contains no geometry");
          return;
        }

        // Create a marker for each place.
        self.mmarker = new google.maps.Marker({
          position: place.geometry.location,
          map: self.map,
          draggable: true,
          title: 'Destination'
        });

        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      self.map.fitBounds(bounds);
    });
  }


  getGeoAddress(coords) {
    var self = this;
    var geocoder = new google.maps.Geocoder;
    var latlng = coords;
    geocoder.geocode({ 'location': latlng }, function (results, status) {
      if (status === 'OK') {
        if (results[0]) {

          self.paddress = results[0].formatted_address;
          this.utility.presentToast(self.paddress);
        } else {
          this.utility.presentToast('No results found');
        }
      } else {
        this.utility.presentToast('Geocoder failed due to: ' + status);
      }
    });

  }

  getDirection() {
    this.utility.openDirectionInMap(this.myAddress);
  }





  getMarkerLocation() {


    var lt = this.mmarker.position.lat();
    var lg = this.mmarker.position.lng();
    var coords = { lat: lt, lng: lg };


    var self = this;
    var geocoder = new google.maps.Geocoder;
    var latlng = coords;
    geocoder.geocode({ 'location': latlng }, function (results, status) {
      if (status === 'OK') {
        // console.log(results);
        if (results[0]) {

          self.paddress = results[0].formatted_address;
          self.utility.presentToast(self.paddress);

          if (self.newAddress == true) {
            var _addressComponets = results[0].address_components;
            // console.log(_addressComponets)
            var countryObject = _addressComponets.filter(x => x.types.includes("country"))[0];
            var country = (countryObject) ? countryObject['long_name'] : ""
            var stateObject = _addressComponets.filter(x => x.types.includes("administrative_area_level_1"))[0];
            var state = (stateObject) ? stateObject['long_name'] : ""
            var cityObject = _addressComponets.filter(x => x.types.includes("administrative_area_level_2"))[0];
            var city = (cityObject) ? cityObject['long_name'] : ""
            var streetObject = _addressComponets.filter(x => x.types.includes("route"))[0];
            var street = (streetObject) ? streetObject['long_name'] : ""

            // console.log(country);


            var threepartAddress = {
              "country": country,
              "state": state,
              "city": city,
              "street": street
            }
            var coords2 = { lat: lt, lng: lg, address: self.paddress, parts: threepartAddress };
            self.closeModal(coords2);
          } else {

            var coords3 = { lat: lt, lng: lg, address: self.paddress };
            self.closeModal(coords3);

          }



        } else {
          self.utility.presentToast('No results found');
        }
      } else {
        self.utility.presentToast('Geocoder failed due to: ' + status);
      }
    });


  }

  closeModal(res) {
    this.modals.dismiss(res);
  }

  calculateAndDisplayRoute() {
    this.directionsService.route({
      origin: this.start,
      destination: this.end,
      travelMode: 'DRIVING'
    }, (response, status) => {
      if (status === 'OK') {
        this.directionsDisplay.setDirections(response);
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    });
  }

}
