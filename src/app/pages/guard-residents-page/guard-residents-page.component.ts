import { QuickPassPageComponent } from './../quick-pass-page/quick-pass-page.component';
import { Component, Injector, OnInit, AfterViewInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-guard-residents-page',
  templateUrl: './guard-residents-page.component.html',
  styleUrls: ['./guard-residents-page.component.scss'],
})
export class GuardResidentsPageComponent extends BasePage implements OnInit, AfterViewInit {

  avatar = this.users.avatar;
  storedData;
  phone_contacts: any[] = [];
  _phone_contacts: any[] = [];
  isIOSPlatform: boolean = true;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {}

  ngAfterViewInit(){
    this.fetchProfileValues();
    this.isIOSPlatform = this.platform.is('ios');

  }

  async fetchProfileValues(){
    this.getGuardResidents();
  }

  formatPhoneNumber(num){
    return this.utility.formatPhoneNumberRuntime(num);
  }

  callMe(num){
    this.utility.dialMyPhone(num);
  }

  quickpassViaAdminGuard(item){
    console.log(item);

    // quick pass params
    // guard_id: get guard id from server bu auth
    // resident_id: item.id
    // Full name of person
    // phone_number

    this.modals.present(QuickPassPageComponent, {resident_id: item.id, display_name: item.fullName });



  }


  getGuardResidents(){
    this.network.getGuardResidents().then((data: any) => {
      this.phone_contacts = data['residents'];
      this._phone_contacts = data['residents'];
    })
  }

  closeModel(){
    this.nav.pop();
  }

  setFilteredItems($event, flag){

    const searchTerm = $event.target.value;

    function housedetailsfilter(item){
      if(item.residents){

        if(item.residents.house_details){
          if(item.residents.house_details != "" && item.residents.house_details.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1){
            return true;
          }
        }

      }

      return false;
    }

    this.phone_contacts = this._phone_contacts.filter(item => {

        return item.fullNameInverted.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
        || item.phone_number.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1
        || housedetailsfilter(item)

    });


  }

}
