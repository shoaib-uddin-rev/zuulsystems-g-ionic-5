import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardPageComponent } from './dashboard-page/dashboard-page.component';
import { GuardResidentsPageComponent } from './guard-residents-page/guard-residents-page.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { SignupComponent } from './signup/signup.component';
import { SplashPageComponent } from './splash-page/splash-page.component';
import { TutorialPageComponent } from './tutorial-page/tutorial-page.component';


const routes: Routes = [
    { path: 'splash', component: SplashPageComponent},
    { path: 'TutorialPage', component: TutorialPageComponent},
    { path: 'LoginPage', component: LoginPageComponent},
    { path: 'SignupPage', component: SignupComponent},
    { path: 'DashboardPage', component: DashboardPageComponent},
    { path: 'GuardResidentsPage', component: GuardResidentsPageComponent},
    {
        path: '',
        redirectTo: 'splash',
        pathMatch: 'full'
    },

];  


@NgModule({
    imports: [
      RouterModule.forChild(routes),
    ],
    exports: [RouterModule]
  })
  export class PagesRoutingModule { }