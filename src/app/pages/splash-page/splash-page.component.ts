import { Component, OnInit, Injector } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { MenuController, Platform } from '@ionic/angular';
import { EventsService } from 'src/app/services/events.service';
import { SqliteService } from 'src/app/services/sqlite.service';
import { UtilityService } from 'src/app/services/utility.service';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-splash-page',
  templateUrl: './splash-page.component.html',
  styleUrls: ['./splash-page.component.scss'],
})
export class SplashPageComponent extends BasePage implements OnInit {

  tapCount = 0;
  skipintro: boolean = false;
  stimeout;
  isSync = false;
  aftertimeout = true;



  constructor( injector: Injector,
    private splashScreen: SplashScreen,
    private menu: MenuController,
  ) {
    super(injector);
    this.platform.ready().then(() => {
      this.splashScreen.hide();
      this.waitTillInitialize();

    });
    this.menu.swipeGesture(false);
    this.init();
  }
  ngOnInit(): void {
    
  }

  async waitTillInitialize(){
    this.sqlite.initialize().then( v => {
      this.callRedirect();
    })
  }

  tap(){
    this.tapCount = this.tapCount + 1;
    setTimeout(function(){
        this.tapCount = 0
      }, 500);
    if(this.tapCount == 2){

      if(!this.skipintro){
        this.skipintro = true;
        this.tapCount = 0;
        this.utility.presentToast("Skipping Intro ...");
        clearTimeout(this.stimeout);
        // this.callRedirect();
      }
    }
  }

  private async init() {

    // get all data from local storage

    var self = this
    this.stimeout =  setTimeout(() => {
      // self.callRedirect();
      self.aftertimeout = false;
    }, 1300);


  }

  async callRedirect(){

    // temporary waiting
    let token = await this.sqlite.getCurrentUserAuthorizationToken();
    this.events.publish('user:get');
  }

  ionViewDidLeave() {
    this.menu.swipeGesture(true);
  }


}
