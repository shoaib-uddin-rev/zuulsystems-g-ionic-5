import { Component, OnInit, AfterViewInit, Injector, Input } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from '../base-page/base-page';
import { GuardLicenceImageComponent } from '../guard-licence-image/guard-licence-image.component';

@Component({
  selector: 'app-quick-pass-page',
  templateUrl: './quick-pass-page.component.html',
  styleUrls: ['./quick-pass-page.component.scss'],
})
export class QuickPassPageComponent extends BasePage implements OnInit, AfterViewInit {

  @Input() resident_id;
  @Input() display_name;
  guard_name;
  user;
  aForm: FormGroup;


  constructor(injector: Injector) { 
    super(injector);
    this.initialize()
    this.setupForm(); 
  }

  ngOnInit() {}

  ngAfterViewInit(){
    
  }

  async initialize(){
    this.user = await this.sqlite.getActiveUser();
    console.log(this.user);
    this.guard_name = this.user.name;
  }

  setupForm(){

    this.aForm = this.formBuilder.group({
      display_name: ['', Validators.compose([Validators.required])],
      phone_number: ['', Validators.compose([Validators.required])],
      visitor_type: ['1', Validators.compose([Validators.required])],

    })

  }

  onTelephoneChange(ev, tel) {
    if (ev.inputType != "deleteContentBackward") {
      var _tel = this.utility.onkeyupFormatPhoneNumberRuntime(tel);
      this.aForm.controls["phone_number"].setValue(_tel);
    }
  }

  validate(){

    var in_name = !this.aForm.controls.display_name.valid && !this.utility.isLastNameExist(this.aForm.controls.display_name.value);
    var in_phone = !this.aForm.controls.phone_number.valid;
    in_phone = !this.utility.isPhoneNumberValid(this.aForm.controls.phone_number.value);

    if(in_name){
      this.utility.presentFailureToast("Name/Full Name is required")
      return false;
    }

    if(in_phone){
      this.utility.presentFailureToast("Phone Number required")
      return false
    }

    return true;

  }

  send(addVehicle = false){

    if(!this.validate()){ return };


    var formdata = this.aForm.value;
    console.log(formdata);
    formdata['resident_id'] = this.resident_id;

    this.network.quickPassViaGuard(formdata).then( res => {
      console.log(res);
      this.modals.dismiss().then( async () => {

        let alertmode = res['alertmode'];
        console.log(alertmode);
        if(alertmode['flag'] == true){
          await this.utility.showAlert(alertmode['message']);
        }

        var data = res['data'];
        data['add_vehicle'] = addVehicle;
        console.log(data);
        this.gotoGuardLicenceImage(data);

      })

    })

  }

  gotoGuardLicenceImage(data){
    console.log(data);
    this.modals.present(GuardLicenceImageComponent, {data: data})
  }

  closeModel(){
    this.modals.dismiss();
  }

}
