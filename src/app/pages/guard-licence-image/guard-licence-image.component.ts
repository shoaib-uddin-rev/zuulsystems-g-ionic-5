import { Component, OnInit, AfterViewInit, Injector, Input, ViewChild } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { IonSlides } from '@ionic/angular';
import { BasePage } from '../base-page/base-page';
import { CreateVehiclePageComponent } from '../create-vehicle-page/create-vehicle-page.component';
import { GuardLicencePageComponent } from '../guard-licence-page/guard-licence-page.component';

@Component({
  selector: 'app-guard-licence-image',
  templateUrl: './guard-licence-image.component.html',
  styleUrls: ['./guard-licence-image.component.scss'],
})
export class GuardLicenceImageComponent extends BasePage implements OnInit, AfterViewInit {

  @Input() data;
  @ViewChild('slides', {static: true}) slides: IonSlides;

  aForm: FormGroup;
  submitAttempt = false;
  liImageBase64 = "";
  profile: any = {};
  user: any = {}
  tempaccesstoken: any;
  date_of_birth_mdy = "";
  revisit: boolean = false;
  islicence_must = false;

  constructor(injector: Injector) { 
    super(injector);
    
    this.setupForm();
  }

  ngOnInit() {}

  ngAfterViewInit() {
    console.log("data on guard app", this.data);
    this.slides.lockSwipes(true);
    if(this.data['add_vehicle'] == true && (!this.data['licence_plate'] || this.data['licence_plate'] == null || this.data['licence_plate'] == "" )){
      this.islicence_must = true;
    }else{
      this.islicence_must = false;
    }

    console.log("after scan", this.data);
    console.log(this.user);
  }

  async setupForm(){
    var re = /\S+@\S+\.\S+/;

    this.aForm = this.formBuilder.group({
      licence_format: ['', Validators.compose([Validators.required])],
      licence_image: ['', Validators.compose([Validators.required])],
      image_verified: [''],
      licence_verified: [''],
    });

    this.user = await this.sqlite.getActiveUser();
  }

  async notVerified(type, flag){
    console.log(type);
    switch(type){

      case 'licence_verified':
        this.aForm.controls.licence_verified.setValue(flag);
        if(!flag){
          const _data = await this.modals.present(CreateVehiclePageComponent, {item: this.data});
          const vehicledata = _data.data;

          console.log(vehicledata);
          if(vehicledata['data'] != 'A'){

            let vehicle = vehicledata;
            delete vehicle['user_id'];
            this.data['licence_plate'] = vehicle['licence_plate'];
            var obj = {...this.data, ...vehicle}
            console.log("combine object" , obj);
            this.network.updateLicencePlateById(this.data.user_id, obj)
            .then( async v => {
              console.log("after update licence", v)
              this.licencePlateThankyou()
            })
          }
        }else{
          this.closeModal({data: 'A'});
        }
      break;

      case 'image_verified':
        this.aForm.controls.image_verified.setValue(flag);
        // if flag is false, upload an image
        if(!flag){
          const _data = await this.modals.present(GuardLicencePageComponent, {user: this.data});
          const data = _data.data;

          if(data['data'] != 'A'){
            this.moveSlide();
          }

        }else{
          this.moveSlide();
        }
      break;
    }
  }
  async licencePlateThankyou(){
    const confirm = await this.utility.showAlert('License plate number uploaded, thank you for cooperation', 'Upload Successful');
    this.closeModal({data: 'A'})
  }

  moveSlide(){
    this.goToNext(1);
  }

  goToNext(num) {

    if(this.slides){
      this.slides.lockSwipes(false);
      this.slides.slideTo(num, 500);
      this.slides.lockSwipes(true);
    }

  }

  closeModal(res) {
    this.modals.dismiss(res);
  }
}