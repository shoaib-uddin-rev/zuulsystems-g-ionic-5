import { BasePage } from 'src/app/pages/base-page/base-page';
import { Component, OnInit, Injector, Input, AfterViewInit, ViewChild } from '@angular/core';
import { IonSearchbar } from '@ionic/angular';

const countries = require('./../../data/countries.json');
@Component({
  selector: 'app-country-code-page',
  templateUrl: './country-code-page.component.html',
  styleUrls: ['./country-code-page.component.scss'],
})
export class CountryCodePageComponent extends BasePage implements OnInit, AfterViewInit {

  item;
  @Input() dial_code;
  @Input() dc;
  
  countries: any[] = [];
  @ViewChild('searchbar', { static: true }) searchbar: IonSearchbar;
  offset = 0;
  search_value;
  search_on = false;

  constructor(injector: Injector) {
    super(injector);

    this.dial_code = this.dc;
    this.countries = countries;
    

  }

  ngAfterViewInit(): void {

    // var d: any = this.countries;
    // // d.forEach(element => {
    // //   element["image"] = 'assets/imgs/flags/' + element.code.toLowerCase() + '.png';
    // // });

    // this.countries = d;
    // this.buffer_countries = d
    // console.log(this.countries);

    this.getCountriesList(null, 0, false, false);
    
  }

  ngOnInit() { }

  setFocusOnSearch(){
    var self = this;
    this.search_on = true;
    setTimeout( () => {
      self.searchbar.setFocus();
    }, 500);

  }


  filterGlobal($event){
    let val = this.search_value;
    if(val == ''){
      val = null
    }
    console.log(val);
    this.getCountriesList(val, this.offset,  false, false);

  }

  resetSearch(){
    this.getCountriesList(null, 0, true, false);
  }

  loadMore($event){
    if(this.offset == -1){
      $event.target.complete();
    }else{
      this.getCountriesList(this.search_value, this.offset, false).then( v => {
        $event.target.complete();
      });
    }
  }

  getCountriesList(search = null, offset, loader = true, is_concat = true) {
    // Reset items back to all of the items
    return new Promise( resolve => {
      this.sqlite.getCountriesInDatabase(search, offset, loader ).then((res: any) => {

        if(is_concat){
          this.offset = res['offset'] as number;
          this.countries = this.countries.concat(res['countries_list']);
        }else{
          this.offset = res['offset'] as number;
          this.countries = res['countries_list'];
        }

        resolve(this.countries);
      }, error => {
        resolve(this.countries);
      });
    });
  }

  onSelectCountry(item){
    this.modals.dismiss(item);
  }

  closeModal(data){
    this.modals.dismiss(data);
  }

}
